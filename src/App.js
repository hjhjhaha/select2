import React from "react";
import Select2 from "./components/select2.jsx";

function App() {
  let arrSelect = [
    {
      label: "Việt Nam",
      value: "vietnam"
    },
    {
      label: "Lào",
      value: "laos"
    },
    {
      label: "Campuchia",
      value: "cambodia"
    },
    {
      label: "Thái Lan",
      value: "thailand"
    },
    {
      label: "Malaysia",
      value: "malaysia"
    },
    {
      label: "Indonesia",
      value: "indonesia"
    },
    {
      label: "Myanma",
      value: "myanma"
    }
  ];
  return (
    <div className="App">
      <Select2 data={arrSelect} isSearch={true} multiple />
    </div>
  );
}

export default App;
