import React from "react";

class Select2 extends React.Component {
  constructor() {
    super();
    this.state = {
      current: {},
      isShow: false,
      listItems: []
    };
  }

  componentDidMount() {
    this.setState({
      current: this.props.data[0],
      listItems: this.props.data
    });
  }

  handleClickOption(item) {
    this.setState({
      current: item,
      isShow: false
    });
  }

  handleClickToggleShow() {
    this.setState({ isShow: !this.state.isShow });
  }

  handleChangeSearch(value) {
    if (value.length) {
      let filterItems = this.state.listItems.filter(item => item.value.indexOf(value) >= 0);
      this.setState({
        listItems: filterItems
      });
    } else {
      this.setState({
        listItems: this.props.data
      });
    }
  }

  render() {
    let _items = this.state.listItems.map((item, key) => (
      <li onClick={e => this.handleClickOption(item)} key={key}>
        {item.value}
      </li>
    ));

    let isSearch = this.props.isSearch ? (
      <div className="d-select__search">
        <input type="text" placeholder="search" onChange={e => this.handleChangeSearch(e.target.value)} />
      </div>
    ) : null;
    return (
      <div className="d-select">
        <div className="d-select__selected" onClick={e => this.handleClickToggleShow()}>
          {this.state.current.value}
        </div>

        {this.state.isShow ? (
          <div className="d-select__items">
            {isSearch}
            <ul>{_items}</ul>
          </div>
        ) : null}
      </div>
    );
  }
}

export default Select2;
